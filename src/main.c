/* Main application logic for a USB to Atari Portfolio Smart Parallel Port
 * file transfer adapter.
 *  - Billy Stevens (wasv)
 */
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include <stdint.h>

#include "pofo.h"

static void gpio_setup(void) {
  // Enable GPIOC clock.
  rcc_periph_clock_enable(RCC_GPIOC);
  // Set GPIO13 (in GPIOC) to 'output push-pull'.
  gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO13);

  gpio_set(GPIOC, GPIO13);
}

int main(void) {
  gpio_setup();
  pofo_setup();

  while (!pofo_init()) {}
  gpio_clear(GPIOC, GPIO13);

  // Blink the LED (PC13) on the board.
  while (1) {}

  return 0;
}
