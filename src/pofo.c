#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include <stdbool.h>
#include <stdint.h>

#include "pofo.h"

#define POFO_PORT GPIOB

// Pin assignments
#define POFO_CLKIN GPIO12
#define POFO_DATIN GPIO13
#define POFO_CLKOUT GPIO14
#define POFO_DATOUT GPIO15

/** Shortcut (a == 0) XNOR (b == 0), to see if both values are zero (LOW), or both are nonzero (HIGH). */
#define MATCH(a, b) (!((a == 0) ^ (b == 0)))

/**
 * Configures preipherals for use with atari parallel port adapter.
 */
void pofo_setup() {
  // Enable GPIOB clock.
  rcc_periph_clock_enable(RCC_GPIOB);

  // Configure GPIOB12 + GPIOB13 as inputs.
  // GPIOB12 = Portfolio Clock (pin 12 of DB25 cable)
  // GPIOB13 = Portfolio Data (pin 13 of DB25 cable)
  gpio_set_mode(POFO_PORT, GPIO_MODE_INPUT, GPIO_CNF_INPUT_PULL_UPDOWN, POFO_CLKIN | POFO_DATIN);

  // Configure GPIOB14 + GPIO15 as outputs.
  // GPIOB14 = PC Clock (pin 3 of DB25 cable)
  // GPIOB15 = PC Data (pin 2 of DB25 cable)
  gpio_set_mode(POFO_PORT, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, POFO_CLKOUT | POFO_DATOUT);

  gpio_clear(POFO_PORT, POFO_DATOUT);
  gpio_clear(POFO_PORT, POFO_CLKOUT);
}

// --- Bit level functions. ---

/**
 * Reads a single bit from the portfolio.
 * @return bit received from portfolio
 */
bool pofo_readbit() {
  // Wait until clock states do not match
  while (MATCH(gpio_get(POFO_PORT, POFO_CLKIN), gpio_get(POFO_PORT, POFO_CLKOUT))) {}

  // Read value.
  bool in_bit = gpio_get(POFO_PORT, POFO_DATIN) != 0;

  // Toggle clock
  gpio_toggle(POFO_PORT, POFO_CLKOUT);
  return in_bit;
}

/**
 * Write a single bit to the portfolio.
 */
void pofo_writebit(bool out_bit) {
  // Write value
  if (out_bit) {
    gpio_set(POFO_PORT, POFO_DATOUT);
  } else {
    gpio_clear(POFO_PORT, POFO_DATOUT);
  }

  // Toggle clock
  gpio_toggle(POFO_PORT, POFO_CLKOUT);

  // Wait until clock state match
  while (!MATCH(gpio_get(POFO_PORT, POFO_CLKIN), gpio_get(POFO_PORT, POFO_CLKOUT))) {}
}

// --- Byte level functions. ---

/**
 * Read a byte from the portfolio.
 * @return byte received from portfolio
 */
uint8_t pofo_readbyte() {
  uint8_t in_byte = 0;
  // Read one bit at a time, least significant bit first.
  for (uint8_t i = 0; i < 8; i++) {
    in_byte |= pofo_readbit() << i;
  }
  return in_byte;
}

/**
 * Writes a byte to portfolio.
 * Byte is written serially, with least significant bit sent first.
 */
void pofo_writebyte(uint8_t out_byte) {
  // Write one bit at a time, least significant bit first.
  for (uint8_t i = 0; i < 8; i++) {
    pofo_writebit((out_byte >> i) & 0x01);
  }
}

// --- Protocol level functions. ---

/**
 * Start communication with portfolio.
 * Assumes pofo_setup has been called.
 * @return true if initialization was acknowledged by portfolio
 */
bool pofo_init() {
  // Set clock high.
  gpio_set(POFO_PORT, POFO_CLKOUT);

  // Wait until clock states match
  while (!MATCH(gpio_get(POFO_PORT, POFO_CLKIN), gpio_get(POFO_PORT, POFO_CLKOUT))) {}

  uint8_t in_byte = pofo_readbyte();
  if (in_byte != 0x5A) { // If device sends a value other than "ready byte"
    // Pull clock low
    gpio_clear(POFO_PORT, POFO_CLKOUT);

    // Wait until clock states match
    while (!MATCH(gpio_get(POFO_PORT, POFO_CLKIN), gpio_get(POFO_PORT, POFO_CLKOUT))) {}

    // Pull clock high
    gpio_set(POFO_PORT, POFO_CLKOUT);

    // Wait until clock states match
    while (!MATCH(gpio_get(POFO_PORT, POFO_CLKIN), gpio_get(POFO_PORT, POFO_CLKOUT))) {}

    return false;
  }
  return true;
}

