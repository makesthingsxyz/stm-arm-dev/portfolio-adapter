Transcribed from Atari Portfolio Technical Reference Guide by Billy Stevens

Section 3.3.1 (INT 61H - DIP Extended BIOS Services)

Fn 30H File Transfer services

Parameters:
AH      30H
AL      0   Transmit block
        1   Receive block
        2   Open ports
        3   Close ports
        4   Wait 500mS
DS:DX   Start of Data buffer
If AL = 0
CX      Bytes to Send
If AL = 1
CX      Maximum buffer size

Returns:
If AL=1
CX      Bytes Received
DL      Error Code
        0   No error
        1   Buffer size too small
        2   Timeout on transmission
        3   Checksum failure
        4   Invalid sub-service
        5   Peripheral not installed

Note:
This is used by the File Transfer utility built into System Setup.