#ifndef POFO_H
#define POFO_H

#include <stdbool.h>
#include <stdint.h>

void pofo_setup(void);

bool pofo_init(void);

bool pofo_readbit(void);

void pofo_writebit(bool out_bit);

uint8_t pofo_readbyte();

void pofo_writebyte(uint8_t out_byte);

#endif // POFO_H
